from time import sleep
from promisesque import Promisesque


"""
Tests
"""


def __test__():
    promisesque = Promisesque(mute_errors=True)

    def action_test():
        sleep(.2)
        return 1

    def action_1_test(arg):
        sleep(.4)
        return arg

    def action_2_test(arg1, arg2):
        sleep(.6)
        return arg1 + arg2

    def break_test(num):
        return "break " + num

    def test_join():
        result = promisesque.join(action_test, (action_1_test, 2), (action_2_test, 1, 2), lambda: break_test(2),)
        test1 = result[0:3] == (1, 2, 3)
        test2 = result[3] == "cannot concatenate 'str' and 'int' objects"
        display_results(False if not test1 or not test2 else True, "BlueJay join")

    def test_run():
        display_results(False, "BlueJay run - no unit tests")

    def display_results(result, test_name):
        if result:
            result_str = "\033[1;30;44m{}\033[0;39;48m {}".format("Passed", test_name)
        else:
            result_str = "\033[1;31;42m{}\033[0;39;48m {}".format("Failed", test_name)
        print result_str

    test_join()
    test_run()

__test__()
