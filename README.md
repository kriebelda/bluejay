BlueJay is a util the allows functions to easily be run asynchronously.  This is beneficial for I/O functions that are not process intensive. 

### How to use ###
```

import promisesque from bluejay
promise = promisesque.Promise()
// what for functions to complete and return results
promise.join((func1, arg1, arg2), (func2, arg1, arg2, arg3))
// do not wait for functions to complete
promise.run((func1, arg1, arg2), (func2, arg1, arg2, arg3))

```