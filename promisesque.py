import threading


class Promise:
    def __init__(self, mute_errors=False):
        self.mute_errors = mute_errors

    def _actions(self, actions):
        threads = []
        thread_results = [None] * len(actions)
        for i, action_ in enumerate(actions):
            try:
                # handles no argument.  So just when a function is using ex: [(function), (function, arg)]
                if not hasattr(action_, "__getitem__"):
                    action_ = [action_, ]
                t = threading.Thread(target=self.action, args=(action_[0], action_[1:], thread_results, i,))
                t.daemon = True  # True forces the thread to die when the main thread dies
                threads.append(t)
            except Exception as e:
                print e
        return threads, thread_results

    def action(self, func, args, results, i):
        try:
            results[i] = func(*args)
        except Exception as e:
            if not self.mute_errors:
                print e
            results[i] = BlueJayError(e)
        return results

    def run(self, *actions):
        """
        This function will thread out a dynamic number of functions
        in the background and NOT wait for them to be completed.

        :param actions: dynamic list of tuples .run((func, arg1, arg2),(func, arg1))
        :return: boolean True to say it completed
        """
        threads, results = self._actions(actions)
        for thread_ in threads:
            thread_.start()
        return True

    def join(self, *actions):

        """
        This function will thread out a dynamic number of functions and waits for them all to be completed.
        The results will be returned in a tuple in the order the functions were sent

        :param actions: dynamic list of tuples .join((func, arg1, arg2),(func, arg1))
        :return: tuple of the results in the order of the functions were sent
        """
        threads, results = self._actions(actions)
        for thread_ in threads:
            thread_.start()

        for thread_ in threads:
            thread_.join()

        return tuple(results)


class BlueJayError:
    def __init__(self, exception_):
        self.type = repr(exception_).split("(")[0]  # this is the only way I was to pull out the error type
        self.message = exception_.message

    def __str__(self):
        return self.message

    def __cmp__(self, other):
        return cmp(self.message, other)
